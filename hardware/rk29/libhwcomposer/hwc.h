/*
 * Copyright (C) 2010 The Android Open Source Project
 * Copyright (C)2012-2013, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _HWC_H_
#define _HWC_H_

#include <hardware/hardware.h>
#include <hardware/hwcomposer.h>
#include "hwc_copybit.h"
#include <hardware/rk_fh.h>
#include "../libgralloc_ump/gralloc_priv.h"

#define MAX_DISPLAYS            (HWC_NUM_DISPLAY_TYPES)
#define MAX_LAYER				3

#define LIKELY( exp )       (__builtin_expect( (exp) != 0, true  ))
#define UNLIKELY( exp )     (__builtin_expect( (exp) != 0, false ))

#define HWC_DEBUG		0
#define OVERLAY_DEBUG 0
struct LayerProp {
	int fd;
	int active;
	int ion_fd;
	int first_set;
};

struct DisplayAttributes {
    uint32_t vsync_period; //nanos
    uint32_t xres;
    uint32_t yres;
    uint32_t stride;
    float xdpi;
    float ydpi;
    uint32_t xres_screen;
    uint32_t yres_screen;
    struct LayerProp layer[MAX_LAYER];
    bool connected; //Applies only to pluggable disp.
    //Connected does not mean it ready to use.
    //It should be active also. (UNBLANKED)
    bool isActive;
    // In pause state, composition is bypassed
    // used for WFD displays only
    bool isPause;
	bool enabledisplay;
};

struct VsyncState {
    bool enable;
    bool fakevsync;
};

struct hwc_context_t {
	hwc_composer_device_1_t device;
	/* our private state goes below here */
	const hwc_procs_t			*procs;
	struct DisplayAttributes		dpyAttr[MAX_DISPLAYS];
	struct VsyncState			vstate;

	CopyBit					*mCopyBit;
	char					mCurmode[32];
	int					overlaymode;
	bool					isModechange;
        int                                     isBlank;
	int					bpp[256];
	private_module_t      *gralloc_module;
	 alloc_device_t        *alloc_device;
};
/*
Define by qiuen@rock-chips.com, 2014-9-18:
TV_COMPOSER_TYPE of enum  is an indentity for hwc compose mode.
ALL_TO_GPU:All of the image layer will be composed by the GPU device.
ALL_TO_LCDC:All of the image layer will be composed by the LCDC device(vop).
            eg.:one layer to the win0 of the vop,another to win1.
MIX_TO_GPU:One image layer will set to the vop(win0 or win1),remaining layer will set to the GPU.

*/
enum TV_COMPOSER_TYPE {
  	ALL_TO_GPU = 0,
  	ALL_TO_LCDC,
  	MIX_TO_GPU,
  	VIDEO_TO_LCDC,
  	THERS = 5,
  	ONE_TO_LCDC = 6,	
};
int hwc_vsync_control(hwc_context_t* ctx, int dpy, int enable);
void init_vsync_thread(hwc_context_t* ctx);
void init_uevent_thread(hwc_context_t* ctx);
void dump_fps(void);
void hwc_dump_layer(hwc_display_contents_1_t* list);
void hwc_set_hdmi_mode(hwc_context_t *ctx, int NewFrameRate);
int hwc_enable_layer(hwc_context_t *ctx, int dpy, int layer, int active);
int hwc_overlay(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src);
int hwc_sprite(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src);
int hwc_overlay_video(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src);
int hwc_overlay_ui(hwc_context_t *ctx,\
                          int dpy,\
                          hwc_layer_1_t *src_layer,\
                          struct rk_fb_win_par *win_par);
int hwc_postfb(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src);
int hwc_yuv2rgb(hwc_context_t *ctx, hwc_layer_1_t *Src);
int openFramebufferDevice(hwc_context_t *ctx);
void hwc_get_screen_size(hwc_context_t *ctx, int dpy);
int hwc_open_cursor(hwc_context_t *ctx);
void hwc_fb_swap(void);

#endif //_HWC_H_
